import { MailboxlayerCheck } from './services/mailboxlayerCheck.services';

const apiProvider = () => ({
  mailboxlayer: () => new MailboxlayerCheck(),
});

export { apiProvider };
