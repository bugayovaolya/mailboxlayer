import faker from 'faker';

const EmailBuilder = function EmailBuilder() {
    this.createEmail = function createEmail ()  {
        this.validEmail = faker.internet.email();
        return this.validEmail;
    };
};

const invalidEmail = {
    user_dots: 'John..Doe@example.com',
    user_dots_first: '.email@example.com',
    user_no: '@example.com',
    domain_no: 'plainaddress',
    domain_incorrect: 'email@example',
    domain_special: 'email@-example.com',
    domain_numbers: 'email@111.222.333.44444',
    domain_dots: 'email@example..com',
};

export { EmailBuilder, invalidEmail };
