import supertest from 'supertest';

import { urls } from '../config/index';

const MailboxlayerCheck = function MailboxlayerCheck() {
    this.checkEmail = async function checkEmail(api_key, email) {
        const r = await supertest(`${urls.mailboxlayer}`)
        .get(`/api/check?access_key=${api_key}&email=${email}`)
        .set('Accept', 'application/json');
        return r;
    };
};

export { MailboxlayerCheck };
