import { apiProvider } from '../framework';
import { describe, expect, test } from '@jest/globals';
import { EmailBuilder, invalidEmail } from '../framework/builder/newEmail';
import { user } from '../framework/config/index';

describe('Проверяем mailboxlayer API', () => {
    test('Позитивный сценарий API', async () => {
        const r = await apiProvider()
            .mailboxlayer()
            .checkEmail(user.mailboxlayer.api_key, new EmailBuilder().createEmail());
        expect(r.body.format_valid).toEqual(true);
    });

    test.each`
    email                             | expected
    ${invalidEmail.user_dots}         | ${false}
    ${invalidEmail.user_dots_first}   | ${false}
    ${invalidEmail.user_no}           | ${false}
    ${invalidEmail.domain_no}         | ${false}
    ${invalidEmail.domain_incorrect}  | ${false}
    ${invalidEmail.domain_special}    | ${false}
    ${invalidEmail.domain_numbers}    | ${false}
    ${invalidEmail.domain_dots}       | ${false}
    `('Невалидные значения API', async ({ email, expected }) => {
        const r = await apiProvider()
            .mailboxlayer()
            .checkEmail(user.mailboxlayer.api_key, email);
        expect(r.body.format_valid).toEqual(expected);
    });

    test('Права доступа к точке без api_key', async () => {
        const r = await apiProvider()
            .mailboxlayer()
            .checkEmail(user.mailboxlayer.emptyApi_key, new EmailBuilder().createEmail());
        expect(r.body.error.code).toEqual(101);
    });
});
